# Raspeberry Pi Lights API

Flask app for controlling Blinkt! LED lights for your Raspberry Pi.

## Hardware Requirements

- Raspberry Pi with a 40 pin-GPIO (all current Raspberry Pi boards support this)
- A [Pimoroni Blinkt!](https://shop.pimoroni.com/products/blinkt) LED hat

## Setup

Plug the Blinkt! leds onto you Raspberry Pi (**Note:** make sure to put them on
the correct way -- the corners should line up with the corners of your Pi).
Clone this repository to your pi, create a virtual environment, and install the
dependencies (if you are testing the web app remove the blinkt line from
requirements.txt).

```bash
git clone git@gitlab.com:chaptuck/light-api.git
cd light-api
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

Test the app!

```
FLASK_APP=app FLASK_ENV=development flask run --host 0.0.0.0
 * Serving Flask app 'app' (lazy loading)
 * Environment: development
 * Debug mode: on
 * Running on http://192.168.1.10:5000/ (Press CTRL+C to quit)
 * Restarting with stat
 * Debugger is active!
 * Debugger PIN: 104-825-923
```

You should now be able to reach the web app by navigating to 'localhost:5000'
on your raspberry pi or on your network at the IP output by the command (in the
above case 192.168.1.10)

## Deploy with systemd

To have the app run on your pi on boot you need to add it to a service manager
(I use systemd but by asking the internet you should be able to translate this
to your favorite service manager). Save the following to
`/etc/systemd/system/light-api.service` (make sure and change the
WorkingDirectory and ExecStart paths)

```
[Unit]
Description=Web application to control blinkt lights
After=network.target

[Service]
User=pi
WorkingDirectory=/PATH/TO/LIGHT-API/PROJECT
ExecStart=/PATH/TO/LIGHT-API/PROJECT/venv/bin/gunicorn -b 0.0.0.0:8000 -w 1 app:app
Restart=always

[Install]
WantedBy=multi-user.target
```

After that is saved start and enable the service

```bash
sudo systemctl daemon-reload
sudo systemctl enable light-api
sudo systemctl start light-api
## Check the status after it starts
sudo systemctl status light-api
● light-api.service - Web application to control blinkt lights
   Loaded: loaded (/etc/systemd/system/light-api.service; enabled; vendor preset: enabled)
   Active: active (running)
 Main PID: 481 (gunicorn)
    Tasks: 2 (limit: 877)
```

Now you should be able to navigate to the web app on your network (the same IP
as before)

![screenshot of web app](docs/imgs/screenshot.png)
