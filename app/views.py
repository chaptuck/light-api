from app import app
from flask import request, jsonify, render_template

try:
    import blinkt
except ModuleNotFoundError:
    class blinkt:
        def set_clear_on_exit(setting):
            pass
        def set_all(r,g,b):
            print(f"\n\tR: {r}\n\tG: {g}\n\tB: {b}\n")
        def show():
            pass


def set_color(r, g, b):
    blinkt.set_clear_on_exit(False)
    blinkt.set_all(r,g,b)
    blinkt.show()


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/color', methods=['GET', 'POST'])
def color():
    data = request.json
    set_color(data['r'], data['g'], data['b'])
    return jsonify({'r': data['r'], 'g': data['g'], 'b': data['b']})

@app.route('/green')
def green():
    set_color(0,255,0)
    return jsonify({'r': 0, "g": 255, "b": 0})

@app.route('/blue')
def blue():
    set_color(0,0,255)
    return jsonify({'r': 0, "g": 0, "b": 255})

@app.route('/red')
def red():
    set_color(255,0,0)
    return jsonify({'r': 255, "g": 0, "b": 0})
